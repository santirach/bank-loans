import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './@views/home/home.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./@views/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'user-create',
    loadChildren: () => import('./@views/user-create/user-create.module').then((m) => m.UserCreateModule),
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule { }

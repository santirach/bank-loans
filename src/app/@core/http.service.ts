import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { interval, Observable } from 'rxjs';
import { finalize, map, take, tap } from 'rxjs/operators';
import { LoaderService } from './loader.service';


@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(
    private readonly _http: HttpClient,
    private readonly ngZone: NgZone,
    private readonly loaderService: LoaderService
  ) { }

  get<T>(endpoint: string, params?: HttpParams): Observable<T> {
    return this.requestWrapper(this._http.get<T>(endpoint, { params }));
  }

  getString(endpoint: string, params?: HttpParams): Observable<string> {
    return this.requestWrapper(this._http.get(endpoint, { params, responseType: 'text' }));
  }

  post<T>(endpoint: string, methodParams: any): Observable<T> {
    return this.requestWrapper(this._http.post<T>(endpoint, methodParams));
  }

  put<T>(endpoint: string, methodParams: any): Observable<T> {
    return this.requestWrapper(this._http.put<T>(endpoint, methodParams));
  }

  patch<T>(endpoint: string, methodParams: any): Observable<T> {
    return this.requestWrapper(this._http.patch<T>(endpoint, methodParams));
  }

  delete<T>(endpoint: string, methodParams?: any): Observable<T> {
    return this.requestWrapper(this._http.delete<T>(endpoint, { params: methodParams }));
  }

  // TODO: remove
  fake<T>(response: T | any): Observable<T> {
    return this.requestWrapper(interval(1500).pipe(map(() => response)));
  }

  // TODO: remove
  errorFake(error: any): Observable<any> {
    return this.requestWrapper(
      interval(1500).pipe(
        tap(() => {
          throw new Error(error);
        })
      )
    );
  }

  private requestWrapper<T>(observer: Observable<T>): Observable<T> {
    this.loaderService.show();
    return observer.pipe(
      finalize(() =>
        this.ngZone.run(() => {
          this.loaderService.hide();
        })
      ),
      take(1)
    );
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  private loaderCounter = 0;
  private readonly _loading = new BehaviorSubject<boolean>(false);
  public readonly loading$ = this._loading.asObservable();

  show() {
    this.loaderCounter++;
    this._loading.next(true);
  }

  hide() {
    this.loaderCounter--;
    if (this.loaderCounter <= 0) {
      this._loading.next(false);
    }
  }
}

import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class CommunicationService {
    static instance: CommunicationService;
    private observable: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    constructor() {
        return CommunicationService.instance = CommunicationService.instance || this;
    }

    returObservable() {
        return this.observable
    }

    emmitValue(value: number) {
        this.observable.next(value);
    }
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DesingModule } from '../@core/design.module';
import { MoneyLendingComponent } from './components/money-lending/money-lending.component';
import { BaseCapitalComponent } from './components/base-capital/base-capital.component';
import { CommunicationService } from '../@core/communication.service';

@NgModule({
  imports: [CommonModule, DesingModule, ReactiveFormsModule, FormsModule],
  declarations: [
    MoneyLendingComponent,
    BaseCapitalComponent
  ],
  exports: [MoneyLendingComponent, BaseCapitalComponent],
  providers: [CommunicationService]
})
export class SharedModule { }

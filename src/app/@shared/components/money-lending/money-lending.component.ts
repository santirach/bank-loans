import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateValidator } from '../../custom/validations/date.validation';
import { formValidationResponse } from './models/money-lending.interface';

@Component({
  selector: 'app-money-lending',
  templateUrl: './money-lending.component.html',
  styleUrls: ['./money-lending.component.css']
})
export class MoneyLendingComponent implements OnInit {
  moneyLendingForm!: FormGroup;
  isSubmitted: boolean = false;
  @Output() stateFormEmmiter = new EventEmitter<formValidationResponse>();

  constructor(private readonly formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.moneyLendingForm = this.formBuilder.group({
      requestedValue: ["", [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      date: ["", [DateValidator()]],
    })

    this.moneyLendingForm.valueChanges.subscribe((status) => {
      this.stateFormEmmiter.emit({ errors: this.findInvalidControlsRecursive(), values: status })
    })
  }

  get form() {
    return this.moneyLendingForm.controls
  }

  private findInvalidControlsRecursive(): string[] {
    var invalidControls: string[] = [];
    Object.keys(this.moneyLendingForm.controls).forEach((field) => {
      const control = this.moneyLendingForm.get(field);
      if (control?.invalid) {
        invalidControls.push(field);
      }
    });
    return invalidControls;
  }

  onReset(): void {
    this.isSubmitted = false
    this.moneyLendingForm.reset()
  }

}

export interface formValidationResponse {
    errors: string[],
    values: {
        date: string,
        requestedValue: string
    }
}
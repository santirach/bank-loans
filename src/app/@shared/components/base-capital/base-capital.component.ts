import { Component, OnInit } from '@angular/core';
import { CommunicationService } from 'src/app/@core/communication.service';
import { UserCreateHttpService } from 'src/app/@views/user-create/services/user-create-http.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-base-capital',
  templateUrl: './base-capital.component.html',
  styleUrls: ['./base-capital.component.css']
})
export class BaseCapitalComponent implements OnInit {
  baseCapitalData: number = environment.baseCapital;
  totalDebt: number = 0;
  constructor(private readonly userCreateHttpService: UserCreateHttpService, private readonly communicationService: CommunicationService) { }

  ngOnInit(): void {
    this.communicationService.returObservable().subscribe(() => {
      this.updateValueCapital()
    })
  }


  updateValueCapital() {
    this.baseCapitalData = environment.baseCapital;
    this.totalDebt = 0;
    this.userCreateHttpService.getUsers().subscribe((users) => {
      users.filter((user) => user.isApproved && !user.isCanceled).map((user) => this.totalDebt = this.totalDebt + parseInt(user.requestedValue.toString()))
      this.baseCapitalData = this.baseCapitalData - this.totalDebt
    })
  }

}

import { FormControl } from "@angular/forms";
import * as moment from "moment";

export function DateValidator(format = "dd/mm/yyyy"): any {
    return (control: FormControl): any => {
        const value = moment(control.value).format(format);
        const val = moment(value, format, true);
        if (control.value == "") {
            return null
        }
        if (!val.isValid()) {
            return { invalidDate: true };
        }

        return null;
    };
}
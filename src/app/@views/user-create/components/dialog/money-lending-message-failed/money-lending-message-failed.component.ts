import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-money-lending-message-failed',
  templateUrl: './money-lending-message-failed.component.html',
  styleUrls: ['./money-lending-message-failed.component.css']
})
export class MoneyLendingMessageFailedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyLendingMessageFailedComponent } from './money-lending-message-failed.component';

describe('MoneyLendingMessageFailedComponent', () => {
  let component: MoneyLendingMessageFailedComponent;
  let fixture: ComponentFixture<MoneyLendingMessageFailedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoneyLendingMessageFailedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyLendingMessageFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

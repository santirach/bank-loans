import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyLendingMessageComponent } from './money-lending-message.component';

describe('MoneyLendingMessageComponent', () => {
  let component: MoneyLendingMessageComponent;
  let fixture: ComponentFixture<MoneyLendingMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoneyLendingMessageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyLendingMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

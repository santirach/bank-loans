import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommunicationService } from 'src/app/@core/communication.service';
import { formValidationResponse } from 'src/app/@shared/components/money-lending/models/money-lending.interface';
import { UserCreateModel, UserCreateModelPost } from './interfaces/user-create.interfaces';
import { UserCreateHttpService } from './services/user-create-http.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MoneyLendingMessageComponent } from './components/dialog/money-lending-message/money-lending-message.component';
import { MoneyLendingMessageFailedComponent } from './components/dialog/money-lending-message-failed/money-lending-message-failed.component';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  registerForm!: FormGroup;
  isSubmitted: boolean = false;
  moneyLendigData!: formValidationResponse;
  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly route: Router,
    private readonly userCreateHttpService: UserCreateHttpService,
    private readonly communicationService: CommunicationService,
    private readonly dialog: MatDialog) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      dni: ["", [Validators.required, Validators.min(7), Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]]
    })
  }


  get form() {
    return this.registerForm.controls
  }

  onSubmit(): void {
    this.isSubmitted = true
    if (this.registerForm.invalid || (this.moneyLendigData && this.moneyLendigData.errors.length > 0)) {
      return
    }
    const request: UserCreateModelPost = {
      ... this.registerForm.value,
      ...this.moneyLendigData.values,
      isApproved: Math.random() <= 0.5,
      isCanceled: false,
    }

    if (parseInt(request.requestedValue.toString()) > parseInt(environment.baseCapital.toString())) {
      this.openDialog(MoneyLendingMessageFailedComponent)
      return
    }

    this.userCreateHttpService.postUser(request).subscribe((data: UserCreateModel) => {
      if (data.isApproved) {
        this.openDialog(MoneyLendingMessageComponent)
        this.communicationService.emmitValue(0)
      } else {
        this.openDialog(MoneyLendingMessageFailedComponent)
      }
    })
  }

  onReset(): void {
    this.isSubmitted = false
    this.registerForm.reset()
    this.redirectTo('/home')
  }

  stateFormEmmiter(event: formValidationResponse): void {
    this.moneyLendigData = event
  }

  private openDialog(componentMessage: any) {
    const dialogRef = this.dialog.open(componentMessage);

    dialogRef.afterClosed().subscribe(() => {
      this.onReset()
    });
  }

  private redirectTo(legacyName: string): void {
    this.route.navigate([legacyName]);
  }

}

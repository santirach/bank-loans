import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/@core/http.service';
import { environment } from 'src/environments/environment';
import { UserCreateModel, UserCreateModelPost } from '../interfaces/user-create.interfaces';

@Injectable({
  providedIn: 'root'
})
export class UserCreateHttpService {

  constructor(private readonly httpService: HttpService) { }

  getUsers(): Observable<UserCreateModel[]> {
    return this.httpService.get(`${environment.apiUrl}/users`);
  }

  postUser(user: UserCreateModelPost): Observable<UserCreateModel> {
    return this.httpService.post(`${environment.apiUrl}/users`, user);
  }

  updateValue(user: UserCreateModel): Observable<UserCreateModel> {
    return this.httpService.put(`${environment.apiUrl}/users/${user.id}`, user);
  }

}

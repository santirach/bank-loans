import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserCreateRoutingModule } from './user-create-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserCreateComponent } from './user-create.component';
import { DesingModule } from 'src/app/@core/design.module';
import { SharedModule } from 'src/app/@shared';
import { CommunicationService } from 'src/app/@core/communication.service';
import { MoneyLendingMessageComponent } from './components/dialog/money-lending-message/money-lending-message.component';
import { MoneyLendingMessageFailedComponent } from './components/dialog/money-lending-message-failed/money-lending-message-failed.component';


@NgModule({
  declarations: [UserCreateComponent, MoneyLendingMessageComponent, MoneyLendingMessageFailedComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    DesingModule,
    UserCreateRoutingModule,
  ],
  providers: [CommunicationService]
})
export class UserCreateModule { }

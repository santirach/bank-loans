export interface UserCreateModel {
    id: number;
    name: string,
    email: string,
    dani: number,
    requestedValue: number,
    date: any,
    isApproved: boolean,
    isCanceled: boolean
}

export interface UserCreateModelPost {

    name: string,
    email: string,
    dani: number,
    requestedValue: number,
    date?: any,
    isApproved: boolean,
    isCanceled: boolean
}



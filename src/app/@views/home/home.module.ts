import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { DesingModule } from 'src/app/@core/design.module';
import { HomeComponent } from './home.component';



@NgModule({
  declarations: [HomeComponent],
  imports: [
    DesingModule,
    CommonModule,
    HomeRoutingModule
  ],
  exports: []
})
export class HomeModule { }

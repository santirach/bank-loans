import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommunicationService } from 'src/app/@core/communication.service';
import { environment } from 'src/environments/environment';
import { UserCreateModel } from '../user-create/interfaces/user-create.interfaces';
import { UserCreateHttpService } from '../user-create/services/user-create-http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  usersCreditDebt!: UserCreateModel[];
  actuallyCurrent: number = environment.baseCapital
  constructor(
    private readonly route: Router,
    private readonly userCreateHttpService: UserCreateHttpService,
    private readonly communicationService: CommunicationService) { }

  ngOnInit(): void {
    this.getUser()
  }

  getUser(): void {
    this.userCreateHttpService.getUsers().subscribe((users) => {
      this.usersCreditDebt = users.filter((user) => user.isApproved && !user.isCanceled)
      this.communicationService.emmitValue(0)
    })
  }

  redirectTo(legacyName: string): void {
    this.route.navigate([legacyName]);
  }

  updateDebet(item: UserCreateModel) {
    const request: UserCreateModel = {
      ...item,
      requestedValue: 0,
      isCanceled: true
    }
    this.userCreateHttpService.updateValue(request).subscribe(() => {
      this.getUser()
    })
  }
}

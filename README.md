#BANK-LOANS (Frontend).

#¡Recomendaciones importante para correr el proyecto!

1.  En la terminal ejecutar el comando npm install.
2.  En una terminal ejecutar el comando npm run backend.
3.  En una segunda terminal ejecutar el comando ng serve.
4.  Tener disponible el puerto 3000 y 4200 en esos puertos corre el json-server y el proyecto en angular.
5.  Una vez ejecutado los comandos ingresar a la url http://localhost:4200/home

#¡Practicas utilizadas en el proyecto!

1.  Se uso angular framework.
2.  Estructura del proyecto  modular.
3.  Uso de interfaces y/o modelos.
4.  Uso de lazyload.
5.  Uso de Angular Material
6.  Reutilizacion de componentes

